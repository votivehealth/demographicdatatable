# README #


### What is this repository for? ###

* Generate a table containing demographic information which is included with the County Landscape charts
* Version 1.0

### How do I get set up? ###

* Just run CreateDemographicTable.py
* Dependencies: xlswriter and pandas


### When is the data updated?

* Once the 2020 census data is published (by Dec 2021) the API calls and county rural table url will need to be updated


### Who do I talk to? ###

* Author: Peter Bordokoff
