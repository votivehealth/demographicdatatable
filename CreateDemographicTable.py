def allCountyDemographics():
    import pandas as pd
    import tkinter
    from tkinter import filedialog
    '''Create an excel file from a DataFrame for all fips codes with the following fields: 
    population, median household income, county name, fips codes, and percent urban'''
   
    # POPULATION (df1)
    popdata = 'https://api.census.gov/data/2019/pep/population?get=NAME,POP&for=county:*&in=state:*'
    df = pd.read_json(popdata)
    # turn row values to column headers, remove offending row
    df1 =  df.rename(columns=df.iloc[0])
    df1.drop(df1.index[0], inplace=True)
    # combine state and county strings and make new column
    df1['fips'] = df[2] + df[3]
    # drop the state and county columns
    df1.drop(['state', 'county'], axis=1, inplace=True)
    # INCOME (df2)
    incdata = 'https://api.census.gov/data/timeseries/poverty/saipe?get=SAEPOVALL_PT,SAEMHI_PT,NAME,GEOID&for=county:*&in=state:*&time=2019'
    df2 = pd.read_json(incdata)
    df2.drop([ 0, 2, 4, 5, 6], axis=1, inplace=True)
    df2 = df2.rename(columns=df2.iloc[0])
    df2.drop(df2.index[0], inplace=True)
    # merge on df1 keys
    df1 = df1.merge(df2, how='outer', left_on='fips', right_on='GEOID')
    # PERCENT URBAN (df3)
    lookuptable = 'https://www2.census.gov/geo/docs/reference/ua/County_Rural_Lookup.xlsx'
    df3 = pd.read_excel(lookuptable, skiprows=3, usecols=['2015 GEOID', '2010 Census \nPercent Rural'])
    # merge df3
    df1 = df1.merge(df3, how='outer', left_on='fips', right_on='2015 GEOID')
    # calculate the percent urban from rural
    df1['Percent Urban'] = 100 - df1['2010 Census \nPercent Rural']
    # drop unnecessary rows
    df1.drop(['GEOID', '2015 GEOID', '2010 Census \nPercent Rural'], axis=1, inplace=True)
    # rename columns
    df1.rename(columns={'SAEMHI_PT':'Median Household Income', 'NAME':'County Name', 'POP':'Population', 'fips':'FIPS Code'}, inplace=True)
    # sort by fips
    df1.sort_values(by='FIPS Code', inplace=True)
    df1['Population'] = pd.to_numeric(df1['Population'])
    df1['Median Household Income'] = pd.to_numeric(df1['Median Household Income'])
    final = df1.convert_dtypes(convert_integer=True)
    # get outfile name from user
    tkObj = tkinter.Tk()
    outxl = tkinter.filedialog.asksaveasfilename(title='Please name the file and where to save it!', parent=tkObj, initialfile='CountyDemographics.xlsx')
    tkObj.destroy()
    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter(outxl, engine='xlsxwriter')
    # Convert the dataframe to an XlsxWriter Excel object.
    final.to_excel(writer, sheet_name='Sheet1', index=False)
    workbook  = writer.book
    worksheet = writer.sheets['Sheet1']
    # add cell formatting
    format1 = workbook.add_format({'num_format': '#,###'})
    format3 = workbook.add_format({'num_format': '$#,###'})
    # Set the column width and formats
    worksheet.set_column('A:A', 40)
    worksheet.set_column('C:C', 10)
    worksheet.set_column('B:B', 10, format1)
    worksheet.set_column('E:E', 15)
    worksheet.set_column('D:D', 28, format3)
    writer.save()
    workbook.close()
    
    
allCountyDemographics() 